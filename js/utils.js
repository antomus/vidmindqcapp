window.vidMindQC = window.vidMindQC || {};
window.vidMindQC.utils = {
  getById: function(id){
    return document.getElementById(id);
  },

  getByQ: function(query){
    return document.querySelector(query);
  },

  getByQAll: function(query){
    return document.querySelectorAll(query);
  },

  // A-> $http function is implemented in order to follow the standard Adapter pattern
  http: function(url){
    try{
    // A small example of object
    var core = {
      
      // Method that performs the ajax request
      ajax : function(method, url, args){
        
        // Establishing a promise in return
        return new Promise(function(resolve, reject) {
              
        // Instantiates the XMLHttpRequest
        var client = new XMLHttpRequest();
        var uri = url;

        if(args && args.data &&(method === 'POST' || method === 'PUT')) {
          url += '?';
          for (var key in args.data) {
            if (args.data.hasOwnProperty(key)) {
              uri += encodeURIComponent(key) + '=' + encodeURIComponent(args.data[key]) + '&';
            }
          }
        }

        if(args && args.headers) {
          for (var key in args.headers) {
            if (args.headers.hasOwnProperty(key)) {
              client.setRequestHeader(key, args.headers[key]);
            }
          }
        }

       try{
          client.open(method, url);
          client.send();

        client.onload = function () {
          if(this.status == 200){
            // Performs the function "resolve" when this.status is equal to 200
            resolve(this.response);
          } else{
            // Performs the function "reject" when this.status is different than 200
            reject(this.statusText);
          }
        };

        client.onerror = function () {
          reject(this.statusText);
        };
      } catch(err) {
        console.log(err);
      }

      });
    }};

    // Adapter pattern
    return {
      'get' : function(args) {
        return core.ajax('GET', url, args);
      },
      'post' : function(args) {
        return core.ajax('POST', url, args);
      },
      'put' : function(args) {
        return core.ajax('PUT', url, args);
      },
      'delete' : function(args) {
        return core.ajax('DELETE', url, args);
      }
    }
  } catch(err) {
    console.log(err);
  }
  },
  sleep: function(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}
}