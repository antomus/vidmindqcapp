window.vidMindQC = window.vidMindQC || {};
window.vidMindQC.optionsPage = {

  saveOptions: function(callback) {
    var self = this;
    var util = window.vidMindQC.utils;
    var url = util.getById('server-url').value;
    var deviceType = util.getById('device-type').value;
    var providerId = util.getById('service-provider-id').value;
    var login = util.getById('login').value;
    var password = util.getById('password').value;
    var groupLimit = util.getById('group-limit').value;
    var episodeLimit = util.getById('episode-limit').value;
    chrome.storage.local.set({
      url: url,
      deviceType: deviceType,
      providerId: providerId,
      login: login,
      password: password,
      groupLimit: groupLimit,
      episodeLimit: episodeLimit
    }, function() {
      callback && callback();
      // Update status to let user know options were saved.
      var status = util.getById('main-header');
      status.textContent = 'Options are saved.';
      setTimeout(function() {
        status.textContent = status.dataset.value;
      }, 750);
    });
  },

  restoreOptions: function() {
    var self = this;
    var util = window.vidMindQC.utils;
    var status = util.getById('main-header');
      status.textContent = 'The data is being fetched ...';
    chrome.storage.local.get({
      url: "",
      deviceType: "",
      providerId: "",
      login: "",
      password: "",
      groupLimit: 5,
      episodeLimit: 5
    }, function(items) {
      try{
        util.getById('server-url').value = items.url;
        util.getById('device-type').value = items.deviceType;
        util.getById('service-provider-id').value = items.providerId;
        util.getById('login').value = items.login;
        util.getById('password').value = items.password;
        util.getById('group-limit').value = items.groupLimit;
        util.getById('episode-limit').value = items.episodeLimit;
        status.textContent = status.dataset.value;
      } catch(err) {
        console.log(err.stack)
      }
    });
  },

  formIsValid: function() {
    var self = this;
    var util = window.vidMindQC.utils;
    var elems = util.getByQAll("#settings-form input[type='text'], input[type='url']");
    for (var i = elems.length - 1; i >= 0; i--) {
      if(!elems[i].checkValidity()) {
        elems[i].classList.add("red-border");
        return false;
      }
      elems[i].classList.remove("red-border");
    }
    return true;
  },

  loginToWF: function(url, deviceType, providerId, name, password) {
    var self =this;
    var util = window.vidMindQC.utils;
    var xhr = new XMLHttpRequest;
    var realDevices = ["tablet","mobile","stb"];
    xhr.open("POST",
              url+"/vidmind-stb-ws/authentication/login",
              true);
    xhr.setRequestHeader("x-vidmind-device-type",deviceType);
    xhr.setRequestHeader("x-vidmind-device-id",1);
    xhr.setRequestHeader("x-vidmind-locale","en");
    xhr.setRequestHeader("x-vidmind-api-key",123456789);
    if(realDevices.indexOf(deviceType) > -1) {
      xhr.setRequestHeader("x-http-mac-address",123456789);
    } else {
      xhr.setRequestHeader("x-vidmind-device-nickname",123456789);
    }
    
    xhr.setRequestHeader("'x-vidmind-app-version'",1);
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send(encodeURI("username="+providerId+"\\"+name+"&password="+password));
    util.getById('main-header').textContent = "Sending request to WF ...";
    xhr.onload = function(){
      if(this.status == 200) {
        self.saveOptions(function(){chrome.runtime.sendMessage({start_api: true}, function(response){
        });
      });
        
      } else {
        var status = util.getById('main-header');
        status.textContent = 'Network error status: '+this.status;
        status.classList.add("red");
        setTimeout(function(){
          status.classList.remove("red");
          status.textContent = 'Settings';
        },1000);
        chrome.runtime.sendMessage({api_error: self.responseText}, function(response) {});
      }
      //var json = JSON.parse(this.responseText);
      
    };
    xhr.onerror = function(){
      var status = util.getById('main-header');

      status.textContent = 'Network error';
      status.classList.add("red");
      setTimeout(function(){
        status.classList.remove("red");
        status.textContent = 'Settings';
      },1000);
    };

  },

  initHandlers: function(){
    var self = this;
    var util = window.vidMindQC.utils;
    self.restoreOptions();
    util.getById("save-button").addEventListener("click", function(evt){
      evt.preventDefault();
      if(self.formIsValid()) {
        var url = util.getById('server-url').value;
        var deviceType = util.getById('device-type').value;
        var providerId = util.getById('service-provider-id').value;
        var name = util.getById('login').value;
        var password = util.getById('password').value;
        self.loginToWF(url, deviceType, providerId, name, password);
      }
    }); 
  },

  init: function(){
    this.initHandlers();
  }
};

document.addEventListener('DOMContentLoaded', function(){
  vidMindQC.optionsPage.init();
});