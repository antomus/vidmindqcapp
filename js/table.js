window.vidMindQC = window.vidMindQC || {};
window.vidMindQC.tablePage = {
  devices: {
        "web" : "WEB",
        "mobile": "Android",
        "tablet": "Iphone / Ipad",
        "stb": "STB"
  },
  currentDevice: null,
  sendAPIstart: function() {
    var self =this;
    chrome.runtime.sendMessage({start_api: true, device: self.currentDevice}, function(response) {});
  },
  setStatus: function(status) {
    var util = window.vidMindQC.utils;
    var elProgr = util.getById("in-progress");
    if(elProgr) {
      elProgr.textContent = status;
    }
  },
  initHandlers: function() {
    var self = this;
    var util = window.vidMindQC.utils;
    util.getById("refresh").addEventListener('click', function(evt){
      self.sendAPIstart();
    });
    chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
      if (request.apiStarted) {
        self.setStatus("API in progress ...");
      } else if (request.apiFinished) {
        self.setStatus("");
        self.clearData();
        self.getAllData();
      } else if (request.api_error) {
        console.log(request.api_error)
        self.setStatus(request.api_error);
        util.getById("in-progress").classList.add("red");
        setTimeout(function(){
         self.setStatus(""); 
         util.getById("in-progress").classList.remove("red");
        }, 4000);
      }
    });
    self.setSelectsHandlers();
  },
  setOptionTextToClipBoard: function(options, index){
    var self =this;
    var util = window.vidMindQC.utils;

    if(options.length > 0) {
      var copyFrom = util.getById("copy");
      copyFrom.textContent = options[index].textContent;
      copyFrom.select();
      document.execCommand('copy');
      copyFrom.textContent = "";
    }
  },
  setSelectsHandlers: function(){
    var util = window.vidMindQC.utils;
    var typeList = ["free","avod","tvod","svod","notAllowed","restricted"];
    var self = this;
    typeList.forEach(function(name){
      if(name !== "restricted") {
        util.getByQ("#"+name+"-title-episode select").addEventListener('focus', function(e){
          self.setOptionTextToClipBoard(e.target.options, e.target.selectedIndex);
        });
        util.getByQ("#"+name+"-title-episode select").addEventListener('change', function(e){
          self.setOptionTextToClipBoard(e.target.options, e.target.selectedIndex);
          var data = JSON.parse(util.getByQ("#data-table-episode").dataset.asset)[name][e.target.selectedIndex];
          if(!data) return;
          e.target.parentNode.parentNode.querySelector("[id$='-tvshow-episode']").textContent = data.seriesName;
          e.target.parentNode.parentNode.querySelector("[id$='-season-number-episode']").textContent = data.seasonNumber;
          e.target.parentNode.parentNode.querySelector("[id$='-content-group-episode']").textContent = data.contentGroup;
        });
      }
      util.getByQ("#"+name+"-title-movie select").addEventListener('focus', function(e){
        self.setOptionTextToClipBoard(e.target.options, e.target.selectedIndex);
      });
      util.getByQ("#"+name+"-title-movie select").addEventListener('change', function(e){
        self.setOptionTextToClipBoard(e.target.options, e.target.selectedIndex);
        var data = JSON.parse(util.getByQ("#data-table-movie").dataset.asset)[name][e.target.selectedIndex];
        if(!data) return;
        e.target.parentNode.parentNode.querySelector("[id$='-content-group-movie']").textContent = data.contentGroup;
      });
    });
  },
  clearData: function(){
    var util = window.vidMindQC.utils;
    var typeList = ["free","avod","svod","notAllowed"];
    typeList.forEach(function(name){
      if(name !== "restricted") {
        util.getByQ("#data-table-episode").dataset.asset = "";
        util.getByQ("#"+name+"-title-episode select").innerHTML = "";
        util.getById(name+"-tvshow-episode").textContent = "";
        util.getById(name+"-season-number-episode").textContent = "";
        util.getById(name+"-content-group-episode").textContent = "";
      }
      util.getByQ("#data-table-movie").dataset.asset = "";
      util.getByQ("#"+name+"-title-movie select").innerHTML = "";
      util.getById(name+"-content-group-movie").textContent = "";
    });
  },
  getAllData: function() {
    var self =this;
    var util = window.vidMindQC.utils;
    chrome.storage.local.get({
      url: "",
      deviceType: "",
      devices : {}
    }, function(items) {
      try{
        console.log("getAllData",items)
        self.currentDevice = items.deviceType.toUpperCase();
        util.getById('current-device').textContent = self.devices[items.deviceType];
        util.getById('current-server').textContent = items.url;
        var noDevice = Object.keys(items.devices).length === 0;
        var noCurrentDevice = !items.devices[self.currentDevice];
        if(!items.url && !items.deviceType) {
          return self.setStatus("Please set up the options");
        } else if (items.url && items.deviceType && (noDevice || noCurrentDevice)) {
          return self.sendAPIstart();
        }
        var episodes = items.devices[self.currentDevice].episode;
        if(episodes) {
          util.getByQ("#data-table-episode").dataset.asset = JSON.stringify(episodes);
          Object.keys(episodes).forEach(function(key){
            if(episodes[key] && episodes[key].length > 0) {
              var epStr = episodes[key].map(function(el){ return "<option>"+el.name+"</option>"}).join();
              util.getByQ("#"+key+"-title-episode select").innerHTML = epStr;
            }
          });
        }
        var movies =  items.devices[self.currentDevice].movie;
        if(movies) {  
          util.getByQ("#data-table-movie").dataset.asset = JSON.stringify(movies);
          Object.keys(movies).forEach(function(key){
            if(movies[key] && movies[key].length > 0) {
              var mvStr = movies[key].map(function(el){ return "<option>"+el.name+"</option>"}).join();
              util.getByQ("#"+key+"-title-movie select").innerHTML = mvStr;
            }
          });
        }
        self.setStatus("");
        self.triggerChangeSelects();
      } catch(err) {
        console.log(err.stack)
      }
    });
  },
  triggerChangeSelects: function(){
    var self = this;
    var util = window.vidMindQC.utils;
    var interval = setInterval(function(){
      var movieOptionTrue = util.getByQAll("#data-table-movie select option").length > 0;
      var episodeOptionTrue = util.getByQAll("#data-table-episode select option").length > 0;
      if(movieOptionTrue && episodeOptionTrue) {
        Array.prototype.slice.call(util.getByQAll("#data-table-movie select")).forEach(function(el){
          el.dispatchEvent(new Event("change"));
        });
        Array.prototype.slice.call(util.getByQAll("#data-table-episode select")).forEach(function(el){
          el.dispatchEvent(new Event("change"));
        });
        window.clearInterval(interval);
      }
    },100);
  },
  init: function() {
    var self = this;
    var util = window.vidMindQC.utils;
    self.initHandlers();
    self.setStatus("The data is being fetched ...");
    self.clearData();
    self.getAllData();
  }
};

document.addEventListener('DOMContentLoaded', function(){
  vidMindQC.tablePage.init();
});