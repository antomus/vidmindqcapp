window.vidMindQC = window.vidMindQC || {};
window.vidMindQC.API = {
  _apiStarted: false,
  endLoop: false,
  offset: 0,
  contentGroupLimit: 5,
  contentGroupOffset: 0,
  episodeOffset: 0,
  episodesLimit: 5,
  assetsList: {
    'SERIES': {},
    'MOVIE': {},
    'EPISODE': {}
  },
  currentDevice: null,
  _apiURL : "",
  _apiPrefix: "/vidmind-stb-ws/",
  movieObject: {'notAllowed':[], 
                'protectedAsset': [], 
                'free': [], 
                'svod': [], 
                'tvod': [], 
                'avod': []},
  episodeObject: {'notAllowed':[], 
                  'protectedAsset': [], 
                  'free': [], 
                  'svod': [], 
                  'tvod': [], 
                  'avod': []},


  getContentArea: function(type, success){
    var util = window.vidMindQC.utils;
    console.log("this._apiURL ", this._apiURL);
    var url =this._apiURL+ this._apiPrefix+"contentareas/"+type+"?includeRestricted=true";
    return util.http(url)
    .get()
    .then(success)
    .catch(function(e) {
      console.log(e.stack); // "oh, no!"
    });
  },

  getContentGroup: function(id,  success){
    var model = this;
    var util = window.vidMindQC.utils;
    //console.log("model.contentGroupLimit ",model.contentGroupLimit);
    //console.log("model.contentGroupOffset ",model.contentGroupOffset);
    var url = this._apiURL+ this._apiPrefix+"gallery/contentgroups/"+id;
      url += "?limit="+model.contentGroupLimit+"&offset="+model.contentGroupOffset;

    return util.http(url)
    .get()
    .then(success)
    .catch(function(e) {
      console.log(e.stack); // "oh, no!"
    });
  },

  getContentGroupsPromieses: function(contentAreas, success) {
    var model = this;
    var res = contentAreas.map(function(a){
      if(a.active) return model.getContentGroup(a.assetId, function(data){
        var assets = JSON.parse(data);
        var mergedRes = [];
        mergedRes = mergedRes.concat.apply(mergedRes, assets);
        mergedRes = mergedRes.map(function(el){
          el['contentGroup'] = a.name;
          return el;
        });
        return mergedRes
      });
    });
    return res;
  },

  getAssetFull: function(type, id, success) {
    var util = window.vidMindQC.utils;
    var typeEnum = {      
      'MOVIE' : 'movie',
      'SERIES' : 'tvs',
      'EPISODE' : 'tve'};
    if(!typeEnum[type]) return "[]";
    var url = this._apiURL+ this._apiPrefix+"assets?"+typeEnum[type]+"="+id;
    return util.http(url)
    .get()
    .then(success)
    .catch(function(e) {
      console.log(e.stack); // "oh, no!"
    });
  },
  prepareTableData: function(){
     console.log("prepareTableData")
    var model = this;
    var res = {};
    var assetTypes = {free: "FREE", svod: "SVOD_ONLY", tvod: "TVOD_ONLY", avod: "ADVERTISED"};

    function addAssetToList(asset, res) {
      if(asset.isGeoblocked || !asset.allowedDevices) return;
      if(model.currentDevice && (asset.allowedDevices.indexOf(model.currentDevice) === -1)) {
        res['notAllowed'].push(asset);
      } else if(asset.protectedAsset) {
        res['protectedAsset'].push(asset);
      }
      else if(asset.paymentLabel){
        if(asset.paymentLabel.type === assetTypes.free) {
          res['free'].push(asset);
        }
        else if(asset.paymentLabel.type === assetTypes.svod) {
          res['svod'].push(asset);
        }
        else if(asset.paymentLabel.type === assetTypes.tvod) {
          res['tvod'].push(asset);
        }
        else if(asset.paymentLabel.type === assetTypes.avod) {
          res['avod'].push(asset);
        }
      }
    }
    Object.keys(model.assetsList.MOVIE).forEach(function(key){
      addAssetToList(model.assetsList.MOVIE[key], model.movieObject);
    });
   
    Object.keys(model.assetsList.EPISODE).forEach(function(key){
      addAssetToList(model.assetsList.EPISODE[key], model.episodeObject);
    });

    var movie = {
                free: vidMindQC.API.movieObject['free'],
                svod: vidMindQC.API.movieObject['svod'],
                tvod: vidMindQC.API.movieObject['tvod'],
                avod: vidMindQC.API.movieObject['avod'],
                restricted: vidMindQC.API.movieObject['protectedAsset'],
                notAllowed: vidMindQC.API.movieObject['notAllowed']
              };
    var episode = {
            free: vidMindQC.API.episodeObject['free'],
            svod: vidMindQC.API.episodeObject['svod'],
            tvod: vidMindQC.API.episodeObject['tvod'],
            avod: vidMindQC.API.episodeObject['avod'],
            notAllowed: vidMindQC.API.episodeObject['notAllowed']
          };

    function addContentGroupsNames(list, series) {
      return list.map(function(el){
        if(list && series[el.seriesId]) {
          el.contentGroup = series[el.seriesId].contentGroup;
        }
        return el;
      });
    }
    episode.free = addContentGroupsNames(episode.free, vidMindQC.API.assetsList.SERIES);
    episode.svod = addContentGroupsNames(episode.svod, vidMindQC.API.assetsList.SERIES);
    episode.tvod = addContentGroupsNames(episode.tvod, vidMindQC.API.assetsList.SERIES);
    episode.avod = addContentGroupsNames(episode.avod, vidMindQC.API.assetsList.SERIES);
    episode.notAllowed = addContentGroupsNames(episode.notAllowed, vidMindQC.API.assetsList.SERIES);

    var data = {devices: {}};
    data.devices[model.currentDevice] = {movie: movie, episode: episode};
    //console.log("prepareTableData",data)
    chrome.storage.local.remove("devices",function(){
      chrome.storage.local.set(data, function() {
        model._apiStarted = false;
        chrome.runtime.sendMessage({apiFinished: true}, function(response){});
      });
    });

  },

  addFieldsToAssets: function(data) {
    var res;
    var model = this;
    //console.log("addFieldsToAssets",data);
    if(Array.isArray(data) && typeof data[0] === "string") {
      res = JSON.parse("["+data.join(", ")+"]");
      var mergedRes = [];
      mergedRes = mergedRes.concat.apply(mergedRes, res);
      res = mergedRes;
    } else {
      res = data;
    }
    //console.log("addFieldsToAssets ",res)
    res.forEach(function(el){
      if(el.assetType === 'MOVIE' && model.assetsList.MOVIE[el.assetId]) {
        if(el.paymentLabel) {
          model.assetsList.MOVIE[el.assetId]['paymentLabel'] = el.paymentLabel;
        }
        if(el.allowedDevices) {
          model.assetsList.MOVIE[el.assetId]['allowedDevices'] = el.allowedDevices;
        }
        if(el.contentGroup) {
          model.assetsList.MOVIE[el.assetId]['contentGroup'] = el.contentGroup;
        }
      } else if(el.assetType === 'EPISODE'  && model.assetsList.EPISODE[el.assetId]) {
        if(el.paymentLabel) {
          model.assetsList.EPISODE[el.assetId]['paymentLabel'] = el.paymentLabel;
        }
        if(el.allowedDevices) {
          model.assetsList.EPISODE[el.assetId]['allowedDevices'] = el.allowedDevices;
        }
        if(el.contentGroup) {
          model.assetsList.EPISODE[el.assetId]['contentGroup'] = el.contentGroup;
        }
      } else if(el.assetType === 'SERIES' && model.assetsList.SERIES[el.assetId]) {
        model.assetsList.SERIES[el.assetId]['seasons'] = el.seasons;
      }
    });
  return res;
  },
 
  parseAssets: function(data){
    var model = this;
    var episodes = [];
    console.log("parseAssets before: ")
    model.addFieldsToAssets(data);
    var keys = Object.keys(model.assetsList.SERIES);
    keys.forEach(function(key){
      episodes.push(model.getEpisodesFromSerie(model.assetsList.SERIES[key]));
    });
    return Promise.all(episodes)
    .then(function(data){
      //setTimeout(function(){model.prepareTableData();}, model.limit.end*1000);
      model.prepareTableData();
    })


  },

  getEpisodesBySeason: function(seriesId, season, success) {
    var util = window.vidMindQC.utils;
    var model = this;
    //console.log("model.episodeLimit ",model.episodeLimit);
    //console.log("model.episodeOffset ",model.episodeOffset);
    var url = this._apiURL+ this._apiPrefix+"gallery/tvgroup/"+seriesId+"?seasonNumber="+season+"&limit="+model.episodeLimit+"&offset="+model.episodeOffset;
    return util.http(url)
    .get()
    .then(success)
    .catch(function(e) {
      console.log(e.stack); // "oh, no!"
    });
  },

  getEpisodesFromSerie: function(series){
    var model = this;
    var util = window.vidMindQC.utils;
    var promises = [];
    if(!series.seasons) return [];
    series.seasons.forEach(function(season){
      promises.push(model.getEpisodesBySeason(series.assetId, season.number, function(data){
        var res = JSON.parse(data);
        res['contentGroup'] = series.contentGroup;
        return res;
      }));
    });
    return Promise.all(promises)
          .then(function(data){
            var episodes = data;
            var mergedRes = [];
            mergedRes = mergedRes.concat.apply(mergedRes, episodes);
            mergedRes.forEach(function(episode){
              model.assetsList.EPISODE[episode.assetId] = episode;
            });
            return model.getEpisodes(mergedRes);
          })
        .catch(function(e) {
          console.log(e.stack); // "oh, no!"
        });

  },

  getEpisodes: function(data) {
    var model = this;
    var promises = [];
    var util = window.vidMindQC.utils;
    data.forEach(function(asset){
      promises.push(model.getAssetFull('EPISODE', asset.assetId, function(data){
        data['contentGroup'] = asset.contentGroup;
        return data;
      }));
    });

    return Promise.all(promises)
      .then(function(data){
        console.log("getEpisodes: ")
        model.addFieldsToAssets(data);
        return data;
      })
    .catch(function(e) {
      console.log(e.stack); // "oh, no!"
    });
  },



  getFullAssets: function(data) {
    var model = this;
    var promises = [];
    var util = window.vidMindQC.utils;
    data.forEach(function(asset){
      promises.push(model.getAssetFull(asset.assetType, asset.assetId, function(data){return data;}));
    });

    return Promise.all(promises)
    .then(function(data){
      model.parseAssets(data);
    })
    .catch(function(e) {
      console.log(e.stack); // "oh, no!"
    });
  },

  getAssets: function(promises) {
    var model = this;
    return Promise.all(promises).then(function(assets){
      var mergedRes = [];
      mergedRes = mergedRes.concat.apply(mergedRes, assets);
      mergedRes.forEach(function(el){
        if(el.assetType === 'MOVIE') {
          model.assetsList.MOVIE[el.assetId] = el;
        } else if(el.assetType === 'SERIES') {
          model.assetsList.SERIES[el.assetId] = el;
        } else if(el.assetType === 'EPISODE') {

        }
      });
      model.getFullAssets(mergedRes);
    })
    .catch(function(err){
      console.error("getAssets ",err.stack);
      this._apiStarted = false;
    });
  },
  clearData: function(){
    var model = this;
    model.movieObject = {'notAllowed':[], 
              'protectedAsset': [], 
              'free': [], 
              'svod': [], 
              'tvod': [], 
              'avod': []};
    model.episodeObject =  {'notAllowed':[], 
                'protectedAsset': [], 
                'free': [], 
                'svod': [], 
                'tvod': [], 
                'avod': []};
    model.assetsList = {
      'SERIES': {},
      'MOVIE': {},
      'EPISODE': {}
    };

  },

  run: function(){
    var model = this;

    model._apiStarted = true;
    chrome.runtime.sendMessage({apiStarted: true}, function(response) {});
    model.clearData();
    chrome.storage.local.get({
      groupLimit: 5,
      episodeLimit: 5,
      url: ""
    }, function(items) {
      
      model._apiURL = items.url;
      model.contentGroupLimit = items.groupLimit;
      model.episodeLimit = items.episodeLimit;
      Promise.all([model.getContentArea("MOVIES", function(data){return data}),
        model.getContentArea("SERIES", function(data){return data;})]).then(function(data){
          var movies = model.getContentGroupsPromieses(JSON.parse(data[0]), function(data){return data});
          var series = model.getContentGroupsPromieses(JSON.parse(data[1]), function(data){return data});
          model.getAssets(movies.concat(series));
        })
        .catch(function(err){
          chrome.runtime.sendMessage({api_error: err}, function(response) {});
          model._apiStarted = false;
        });
    });

  },

  initHandlers: function(){
    var model = this;
    chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
      if (!model._apiStarted && request.start_api && request.device) {
        model.currentDevice = request.device;
        model.run();
      }
    });
  },
  init: function() {
    var model = this;
    model.initHandlers();
  }
}

//chrome.runtime.onInstalled.addListener( function(){
  vidMindQC.API.init();
//});